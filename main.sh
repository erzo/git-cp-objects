#!/usr/bin/env bash

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

usage="\
Usage: $0 SOURCE DEST HASH [HASH ...]
  or:  $0 --recursive SOURCE DEST [HASH ...]
  or:  $0 --branch SOURCE DEST BRANCH

Copy loose objects from one git repository to another.
If the objects you want to copy are packed you can
move the packed objects out of .git/objects/pack and
unpack them with \`git unpack-objects <pack-*.pack\`.

--recursive/-r checks for missing objects with \`git fsck\`
and copies them automatically.
Please note that unreferenced objects are reported as
dangling and not their dependencies as missing. Therefore
--branch/-b copies a reference and implies -r.

This program touches the .git directory directly.
I cannot guarantee that it will work in all cases.
This is meant for the case of emergency, only.
Use at your own risk.

Tested with git version 2.11.0 and 2.25.0.
"

set -o errexit
set -o nounset

function cpobj {
	local src="$srcroot/.git/objects"
	local dst="$dstroot/.git/objects"
	local objects
	local obj

	if [ ! -e "$src" ]; then
		echo >&2 "src $src does not exist"
		return 1
	fi
	if [ ! -e "$dst" ]; then
		echo >&2 "dst $dst does not exist"
		return 1
	fi

	if [ "$#" = 0 ]; then
		mapfile -t objects
	else
		objects=("$@")
	fi

	local returncode=-1
	for obj in "${objects[@]}"; do
		local dn="${obj:0:2}"
		local fn="${obj:2}"
		if [ ! -e "$dst/$dn" ]; then
			mkdir "$dst/$dn"
		fi
		echo cp "$src/$dn/$fn" "$dst/$dn/$fn"
		cp "$src/$dn/$fn" "$dst/$dn/$fn" || return 2
		returncode=0
	done

	return $returncode
}

function cprec {
	if [ "$#" -gt 0 ]; then
		cpobj "$@"
	fi

	while (cd "$dstroot" && git fsck 2>/dev/null) | grep "^missing" | cut -d " " -f 3 | cpobj; do :; done
}

function cpbranch {
	if [ "$#" -lt 1 ]; then
		echo >&2 "ERROR: no branch specified"
		return 1
	fi
	if [ "$#" -gt 1 ]; then
		echo >&2 "ERROR: too many arguments given."
		echo >&2 "Expected exactly one after src and dst: the name of the branch."
		return 1
	fi

	local branch="$1"
	local path=".git/refs/heads"
	local srcfn="$srcroot/$path/$branch"
	local dstpath="$dstroot/$path"
	local dstfn="$dstpath/$branch"

	if [ ! -f "$srcfn" ]; then
		echo >&2 "ERROR: file $srcfn does not exist"
		return 1
	fi
	if [ ! -d "$dstpath" ]; then
		echo >&2 "ERROR: path $dstpath does not exist"
		return 1
	fi
	if [ ! -f "$dstfn" ]; then
		echo >&2 "WARNING: $dstfn does not exist"
	fi

	cp "$srcfn" "$dstfn"
	cprec "`cat "$srcfn"`"

	local returncode=$?
	if [ "$returncode" = "0" ]; then
		echo "You might want to update the index and working directory now."
		echo "You can do that by running:"
		if [ "$dstroot" != "." ]; then
			echo "$ cd '$dstroot'"
		fi
		echo "$ git reset"
		echo "$ git checkout ."
	fi
	return $returncode
}


case "$1" in
'-h' | '--help')
	echo "$usage"
	exit 0
	;;
'-b' | '--branch')
	main=cpbranch
	shift
	;;
'-r' | '--recursive')
	main=cprec
	shift
	;;
*)
	main=cpobj
	;;
esac

srcroot="${1%/}"
dstroot="${2%/}"
shift 2

$main "$@"
