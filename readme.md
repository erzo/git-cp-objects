Recently I corrupted a local git repository by trying to fix the linebreaks which were changed by a code generator with `find . -type f -print0 | xargs -0 sed -i 's/\r$//'`.
At first I did not notice that something was wrong.
`sed` did it's job well and `git diff` showed only a few changes (instead of every line in every file as before).
I committed these changes and continued working until I wanted to push the branch.

The problem was that `find` found all files, including those in the `.git` directory.
And `sed` modified them happily as it was instructed to.
I was not able to push, fetch or stash anymore.

I was able to fix this by copying the missing git objects from the corrupted repository to a new clone of the remote repository
and updating `.git/refs/heads/<branchname>`.

Afterwards `git log` displayed the history correctly
but the working directory was not changed yet
and `git diff` reported all differences to undo the copied commits as staged.
So I finished it off with `git reset; git checkout .`.

This program helps to copy the missing objects to the new clone.

# Usage

You can update an entire branch with:

    ./main.py -b src dst branch

where `src` is the path of the source repository,
`dst` is the path of the destination repository and
`branch` is the name of the branch.
`branch` must exist in `src` but need not exist in `dst`.

The output of `./main.py --help`:

	Usage: ./main.sh SOURCE DEST HASH [HASH ...]
	  or:  ./main.sh --recursive SOURCE DEST [HASH ...]
	  or:  ./main.sh --branch SOURCE DEST BRANCH

	Copy loose objects from one git repository to another.
	If the objects you want to copy are packed you can
	move the packed objects out of .git/objects/pack and
	unpack them with `git unpack-objects <pack-*.pack`.

	--recursive/-r checks for missing objects with `git fsck`
	and copies them automatically.
	Please note that unreferenced objects are reported as
	dangling and not their dependencies as missing. Therefore
	--branch/-b copies a reference and implies -r.

	This program touches the .git directory directly.
	I cannot guarantee that it will work in all cases.
	This is meant for the case of emergency, only.
	Use at your own risk.

	Tested with git version 2.25.0.

# References

- [git objects](https://git-scm.com/book/en/v2/Git-Internals-Git-Objects)
- [loose vs packed objects](https://git-scm.com/book/en/v2/Git-Internals-Packfiles)
- [unpacking packed objects](https://stackoverflow.com/a/16972155)
